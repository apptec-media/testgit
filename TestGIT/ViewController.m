//
//  ViewController.m
//  TestGIT
//
//  Created by Sebastian_Hoffmann on 14.06.15.
//  Copyright (c) 2015 Sebastian_Hoffmann. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *Outlet_Text1;
@property (weak, nonatomic) IBOutlet UILabel *Outlet_Text2;
@property (weak, nonatomic) IBOutlet UITextField *Outlet_Eingabe;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)pushStart:(id)sender {
    _Outlet_Text2.text = _Outlet_Text1.text;
    _Outlet_Text1.text = _Outlet_Eingabe.text;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}

@end
