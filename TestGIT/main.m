//
//  main.m
//  TestGIT
//
//  Created by Sebastian_Hoffmann on 14.06.15.
//  Copyright (c) 2015 Sebastian_Hoffmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
