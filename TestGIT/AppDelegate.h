//
//  AppDelegate.h
//  TestGIT
//
//  Created by Sebastian_Hoffmann on 14.06.15.
//  Copyright (c) 2015 Sebastian_Hoffmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

